#!/usr/bin/env node

var replacements = {};
var medicine;

require('readline').createInterface(process.stdin, null)
.on('line', function(line){

  // Ca => SiRnFYFAr
  // [blank line]
  // medice molecule

  var m = line.match(/^(\w+) => (\w+)$/);
  if (m){
    replacements[m[1]] || (replacements[m[1]] = []);
    replacements[m[1]].push(m[2]);
  }
  else if (line.length){
    medicine = line;
  }

})
.on('close', function(){

  var molecules = {};

  Object.keys(replacements).forEach(function(s){
    replacements[s].forEach(function(r){
      var re = new RegExp(s, 'g');
      var m, t;
      while ((m = re.exec(medicine))){
        t = medicine.slice(0, m.index) + r + medicine.slice(m.index + s.length);
        molecules[t] = true;
      }
    });
  });

  console.log(Object.keys(molecules).length);

});
