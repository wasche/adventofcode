#!/usr/bin/env node

// var inputArray = [1, 2, 3];
//
// var result = inputArray.reduce(function permute(res, item, key, arr) {
//     return res.concat(arr.length > 1 && arr.slice(0, key).concat(arr.slice(key + 1)).reduce(permute, []).map(function(perm) { return [item].concat(perm); }) || item);
// }, []);

function permutator(inputArr) {
  var results = [];

  function permute(arr, memo) {
    var cur, memo = memo || [];

    for (var i = 0; i < arr.length; i++) {
      cur = arr.splice(i, 1);
      if (arr.length === 0) {
        results.push(memo.concat(cur));
      }
      permute(arr.slice(), memo.concat(cur));
      arr.splice(i, 0, cur[0]);
    }

    return results;
  }

  return permute(inputArr);
}

var graph = {};

require('readline').createInterface(process.stdin, null)
.on('line', function(line){

  // David would gain 41 happiness units by sitting next to Carol.
  var m = line.match(/^(\w+) would (gain|lose) (\d+) happiness units by sitting next to (\w+).$/)
    , a = m[1]
    , b = m[4]
    , n = m[3] * (m[2] === 'lose' ? -1 : 1)
    ;

  graph[a] || (graph[a] = {me: 0});
  graph[a][b] = n;

})
.on('close', function(){

  // add yourself
  graph.me = Object.keys(graph).reduce(function(r, n){
    r[n] = 0;
    return r;
  }, {});

  var seats = Object.keys(graph);

  // find all permutations
  var opts = permutator(seats).reduce(function(r, p){
    var w = p.reduce(function(s, n, i, arr){
      return s + (i ?
        graph[n][arr[i-1]] + graph[arr[i-1]][n] :
        graph[n][arr[arr.length-1]] + graph[arr[arr.length-1]][n]
      );
    }, 0);
    (r[w] || (r[w] = [])).push(p);
    return r;
  }, {});

  var max = Object.keys(opts).sort(function(a,b){return a - b}).pop();
  console.log(max);

});
