#!/usr/bin/env node

var pass = process.argv[2];

// optimization:
// find the left most i, o, or l,
// increment it, and replace all following letters with a
var i = pass.search(/([iol])/);
if (i >= 0){
  pass = pass.slice(0, i)
    .concat(String.fromCharCode(pass[i].charCodeAt() + 1))
    .concat(pass.slice(i+1).replace(/\w/g,'a'));
} else {
  pass = advance(pass);
}

function advance(s){
  var ary = s.split('').reverse()
    , i = 0;

  for (; ary[i] == 'z' && i < ary.length; i++){}
  ary[i] = String.fromCharCode(ary[i].charCodeAt() + 1);
  if ('iol'.indexOf(ary[i]) >= 0) String.fromCharCode(ary[i].charCodeAt() + 1);
  ary.fill('a', 0, i);
  return ary.reverse().join('');
}

function valid(s){
  var d = s.match(/([a-z])\1/g) || [].sort().filter(function(item, pos, ary) {
        return !pos || item != ary[pos - 1];
    });
  if (d.length < 2) return false;

  for (i = 2; i < s.length; i++){
    if (s.charCodeAt(i-2) == s.charCodeAt(i)-2 && s.charCodeAt(i-1) == s.charCodeAt(i)-1) return true;
  }

  return false;
}

while (!valid(pass)){
  pass = advance(pass);
};

console.log(pass);
