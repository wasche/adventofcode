#!/usr/bin/env node

var deer = {}
  , time = process.argv[2] && parseInt(process.argv[2]) || 2503
  ;

require('readline').createInterface(process.stdin, null)
.on('line', function(line){

  // Vixen can fly 19 km/s for 7 seconds, but then must rest for 124 seconds.
  var m = line.match(/^(\w+) can fly (\d+) km\/s for (\d+) seconds, but then must rest for (\d+) seconds.$/)
    , r = m[1]
    , speed = parseInt(m[2])
    , fly = parseInt(m[3])
    , rest = parseInt(m[4])
    ;

  deer[r] = {
    speed: speed
  , fly: fly
  , rest: rest
  , points: 0
  };

})
.on('close', function(){

  for (var s = 1; s <= time; s++){
    // find which deer is in the lead
    Object.keys(deer).reduce(function(r, n){
      var o = deer[n]
        , d = Math.floor(s / (o.fly + o.rest)) * o.speed * o.fly +
              Math.min(s % (o.fly + o.rest), o.fly) * o.speed
        ;
      return d < r[0] ? r :
        d > r[0] ? [d, n] :
        r.concat([n]);
    }, [0])
    .slice(1)
    .forEach(function(n){
      deer[n].points++;
    });
  }

  console.log(
    Object.keys(deer)
      .map(function(n){
        return deer[n].points;
      })
      .sort(function(a,b){ return a - b; })
      .pop()
  )

});
