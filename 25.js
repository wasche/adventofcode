#!/usr/bin/env node

var row = 2981;
var col = 3075;

var size = row + col - 1;

var codes = Array.apply(null, {length: size})
.map(function(){
  return Array.apply(null, {length: size});
});

codes[0][0] = 20151125;

var rows = 0;
var cols = 0;

var r = 1;
var c = 0;
var n = codes[0][0];

function next(n){ return (n * 252533) % 33554393; }

while (r < size && c < size){
  n = next(n);
  codes[r][c] = n;
  r--;
  c++;
  if (r < 0){
    r = c;
    c = 0;
  }
}

console.log(codes[row-1][col-1]);
