#!/usr/bin/env node

var grid = [];

function neighbors(x, y){
  var r = [];
  for (var a = Math.max(0, x - 1); a < Math.min(x + 2, grid.length); a++){
    for (var b = Math.max(0, y - 1); b < Math.min(y + 2, grid[x].length); b++){
      if (a === x && b === y) continue;
      r.push(grid[a][b]);
    }
  }
  return r;
}

require('readline').createInterface(process.stdin, null)
.on('line', function(line){

  grid.push(
    line.split('')
      .map(function(c){
        return '#' === c;
      })
  );

})
.on('close', function(){

  var edge = grid.length-1;

  grid[0][0] = true;
  grid[0][edge] = true;
  grid[edge][0] = true;
  grid[edge][edge] = true;

  var rounds = process.argv[2] || 100;

  for (var i = 0; i < rounds; i++){
    grid = grid.map(function(row, x){
      return row.map(function(cell, y){
        var n = neighbors(x, y)
          .filter(function(b){ return b; })
          .length;
        if (
          (x === 0 && y === 0) ||
          (x === 0 && y === edge) ||
          (x === edge && y === 0) ||
          (x === edge && y === edge)
        ) return true; // edges stuck on
        return cell ? (n === 2 || n === 3) : n === 3;
      });
    });
  }

  console.log(
    grid.reduce(function(n, row){
      return n + row.reduce(function(o, cell){
        return cell ? o + 1 : o;
      }, 0)
    }, 0)
  );

});
