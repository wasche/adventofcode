#!/usr/bin/env node

var vowels = 'aeiou';
var blacklist = 'ab cd pq xy'.split(' ');
var double = /([a-z])\1/;

var nice = 0;

require('readline').createInterface(process.stdin, null)
.on('line', function(line){
  var v = line.trim()
    .split('')
    .filter(function(l){ return vowels.indexOf(l) >= 0; })
    .length;
  if (v < 3) return;
  if (!line.trim().match(double)) return;
  if (blacklist.find(function(str){ return line.indexOf(str) >= 0; })) return;
  nice++;
})
.on('close', function(){
  console.log(nice);
});
