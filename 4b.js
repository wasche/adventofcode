#!/usr/bin/env node

var input = process.argv[2];
var md5 = require('md5');
var n = -1;
var hash;

do {
  hash = md5(input + (++n));
} while (hash.substring(0, 6) !== '000000')

console.log(n , hash);
