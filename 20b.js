#!/usr/bin/env node

var input = 33100000;

for (var h = 1; h < input; h++){
  var p = 0;
  for (var e = 1; e <= h; e++){
    if (h / e <= 50 && h % e === 0) p += e * 11;
  }
  if (p >= input){
    console.log(h);
    break;
  }
}
