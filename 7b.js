#!/usr/bin/env node

var Curcuit = function(){};
Curcuit.prototype.test = function(id){
  return this[id] ? (typeof this[id] == 'function' ? (this[id] = this[id]()) : this[id]) : parseInt(id, 10);
};

var wires = new Curcuit();

var gates = {
  AND:    function(a, b){ return function(){ console.log(a, 'and', b); return wires.test(a) & wires.test(b); }; },
  OR:     function(a, b){ return function(){ console.log(a, 'or', b); return wires.test(a) | wires.test(b); }; },
  NOT:    function(a, b){ return function(){ console.log('not', b); return 65535 - wires.test(b); }; },
  LSHIFT: function(a, b){ return function(){ console.log(a, 'lshift', b); return wires.test(a) << wires.test(b); }; },
  RSHIFT: function(a, b){ return function(){ console.log(a, 'rshift', b); return wires.test(a) >> wires.test(b); }; },
  signal: function(a, b){ return function(){ console.log('signal', a); return wires.test(a); }; }
};

require('readline').createInterface(process.stdin, null)
.on('line', function(line){

  var m = line.match(/^((?:\w+| )+) -> (\w+)$/)
    , logic = m[1].match(/^(\w+)? ?(AND|OR|NOT|LSHIFT|RSHIFT) (\w+)$/)
    , input = !logic && m[1] || logic[1]
    , gate = logic && logic[2] || 'signal'
    , operand = logic && logic[3]
    , target = m[2]
    , fn
    ;

  wires[target] = gates[gate](input, operand);

})
.on('close', function(){

  wires.b = 46065;

  console.log(wires.a());

});
