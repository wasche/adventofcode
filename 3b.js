#!/usr/bin/env node
process.stdin.setEncoding('utf8');

var chunk;
var field = {0: {0: 1}};
var coords = [{x: 0, y: 0}, {x: 0, y: 0}];

process.stdin.on('readable', function(){
  if (null !== (chunk = process.stdin.read())){
    chunk.split('').forEach(function(dir){
      var p = coords[0];
      switch (dir){
        case '>':
          p.x++;
          break;
        case '<':
          p.x--;
          break;
        case '^':
          p.y--;
          break;
        case 'v':
          p.y++;
          break;
      }

      field[p.y] || (field[p.y] = []);
      field[p.y][p.x] || (field[p.y][p.x] = 0);

      field[p.y][p.x]++;

      coords.reverse();
    });
  }
});

process.stdin.on('end', function(){

  var houses = 0;

  for (y in field){
    for (x in field[y]){
      houses++;
    }
  }

  console.log(houses);

});
