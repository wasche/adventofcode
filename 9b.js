#!/usr/bin/env node

function permutator(inputArr) {
  var results = [];

  function permute(arr, memo) {
    var cur, memo = memo || [];

    for (var i = 0; i < arr.length; i++) {
      cur = arr.splice(i, 1);
      if (arr.length === 0) {
        results.push(memo.concat(cur));
      }
      permute(arr.slice(), memo.concat(cur));
      arr.splice(i, 0, cur[0]);
    }

    return results;
  }

  return permute(inputArr);
}

var graph = {};

require('readline').createInterface(process.stdin, null)
.on('line', function(line){

  // AlphaCentauri to Snowdin = 66
  var m = line.match(/^(\w+) to (\w+) = (\d+)$/)
    , f = m[1]
    , t = m[2]
    , c = parseInt(m[3], 10)
    ;

  graph[f] || (graph[f] = {});
  graph[t] || (graph[t] = {});

  graph[f][t] = c;
  graph[t][f] = c;

})
.on('close', function(){

  var cities = Object.keys(graph)
    , solutions = permutator(cities)
        .reduce(function(result, path){
          var len = path.reduce(function(n, city, idx, arr){
            if (idx == 0) return n;
            return n + graph[city][arr[idx-1]];
          }, 0);
          result[len] || (result[len] = []);
          result[len].push(path);
          return result;
        }, {})
    , distances = Object.keys(solutions).sort(function(a,b){ return a - b; })
    ;

  console.log(distances.pop());

});
