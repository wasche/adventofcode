#!/usr/bin/env node

var grid = Array.apply(null, {length: 1000})
.map(function(){
  return Array.apply(null, {length: 1000}).fill(0);
});

var commands = {
  'turn on': function(b){ return b + 1; },
  'turn off': function(b){ return Math.max(0, b - 1); },
  'toggle': function(b){ return b + 2; }
};

require('readline').createInterface(process.stdin, null)
.on('line', function(line){
  // turn off 61,44 through 567,111
  // toggle 880,25 through 903,973
  // turn on 347,123 through 864,746

  var arr = line.match(/^(turn off|turn on|toggle) (\d+),(\d+) through (\d+),(\d+)$/)
    , cmd = commands[arr[1]]
    , x1 = parseInt(arr[2], 10)
    , y1 = parseInt(arr[3], 10)
    , x2 = parseInt(arr[4], 10)
    , y2 = parseInt(arr[5], 10)
    ;

  for (var x = x1; x <= x2; x++){
    for (var y = y1; y <= y2; y++){
      grid[x][y] = cmd(grid[x][y]);
    }
  }

})
.on('close', function(){

  var lit = grid.reduce(function(n, arr){
    return n + arr.reduce(function(i, b){
      return b + i;
    }, 0);
  }, 0);

  console.log(lit);

});
