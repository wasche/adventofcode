#!/usr/bin/env node

var codesize = 0, memsize = 0;

require('readline').createInterface(process.stdin, null)
.on('line', function(line){

  var s = line.trim().slice(1, -1)
    , code = s.length + 2
    , m = s.replace(
              /\\(\\|"|x([0-9a-f]{2}))/g,
              function(sub, s1, code){
                return code ? String.fromCharCode(parseInt(code, 16)) : s1;
              })
    , mem = m.length
    ;

  console.log(code, mem, s.replace(/\\/g, '_'), m.replace(/\\/g, '_'));

  codesize += code;
  memsize += mem;

})
.on('close', function(){

  console.log(codesize - memsize);

});
