#!/usr/bin/env node

var nice = 0;

require('readline').createInterface(process.stdin, null)
.on('line', function(line){
  if (!line.trim().match(/([a-z]{2}).*\1/)) return;
  if (!line.trim().match(/([a-z])[^\1]\1/)) return;
  nice++;
})
.on('close', function(){
  console.log(nice);
});
