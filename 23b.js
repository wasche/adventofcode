#!/usr/bin/env node

var prog = [];

require('readline').createInterface(process.stdin, null)
.on('line', function(line){

  var m = line.match(/^(hlf|tpl|inc|jmp|jie|jio) (\w)?,? ?([+-]\d+)?$/);
  if (!m) return;

  prog.push({
    instruction: m[1]
  , register: m[2]
  , offset: parseInt(m[3], 10) || 0
  });

})
.on('close', function(){

  var registers = {
    a: 1
  , b: 0
  };

  var i = -1;

  while (++i < prog.length){
    var cmd = prog[i];
    console.log(i, registers, cmd);
    switch (cmd.instruction){
      case 'hlf':
        registers[cmd.register] /= 2;
        break;
      case 'tpl':
        registers[cmd.register] *= 3;
        break;
      case 'inc':
        registers[cmd.register]++;
        break;
      case 'jmp':
        i += cmd.offset - 1;
        break;
      case 'jie':
        registers[cmd.register] % 2 === 0 && (i += cmd.offset - 1);
        break;
      case 'jio':
        registers[cmd.register] === 1 && (i += cmd.offset - 1);
        break;
      default:
        console.log('Unknown instruction:', cmd.instruction);
    }
  }

  console.log(registers.b);

});
