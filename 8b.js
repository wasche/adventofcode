#!/usr/bin/env node

var orig = 0, encoded = 0;

require('readline').createInterface(process.stdin, null)
.on('line', function(line){

  var s = line.trim()
    , enc = '"' + s.replace(
              /("|\\)/g, '\\$1') + '"'
    ;

  orig += s.length;
  encoded += enc.length;

})
.on('close', function(){

  console.log(encoded - orig);

});
