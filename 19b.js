#!/usr/bin/env node

var replacements = {};
var medicine;

require('readline').createInterface(process.stdin, null)
.on('line', function(line){

  // Ca => SiRnFYFAr
  // [blank line]
  // medice molecule

  var m = line.match(/^(\w+) => (\w+)$/);
  if (m){
    replacements[m[1]] || (replacements[m[1]] = []);
    replacements[m[1]].push(m[2]);
  }
  else if (line.length){
    medicine = line;
  }

})
.on('close', function(){

  // e -> medicine -- shortest path

  var rr = Object.keys(replacements).reduce(function(o, k){
    replacements[k].forEach(function(r){
      o[r] = k;
    });
    return o;
  }, {});

  function fab(molecule, steps){
    if ('e' === molecule) return steps;
    var idx, t, result;

    for (var r in rr){
      while ((idx = molecule.indexOf(r, idx+1)) != -1){
        t = molecule.slice(0, idx) + rr[r] + molecule.slice(idx + r.length);
        result = fab(t, steps + 1);
        if (result !== -1) return result;
      }
    }

    return -1;
  }

  console.log(fab(medicine, 0));

});
