#!/usr/bin/env node
process.stdin.setEncoding('utf8');

var chunk;
var field = {0: {0: 1}};
var x = 0;
var y = 0;
var a = 0;
var b = 0;

process.stdin.on('readable', function(){
  if (null !== (chunk = process.stdin.read())){
    chunk.split('').forEach(function(dir){
      switch (dir){
        case '>':
          x++;
          break;
        case '<':
          x--;
          break;
        case '^':
          y--;
          break;
        case 'v':
          y++;
          break;
      }

      field[y] || (field[y] = []);
      field[y][x] || (field[y][x] = 0);

      field[y][x]++;

    });
  }
});

process.stdin.on('end', function(){

  var houses = 0;

  for (y in field){
    for (x in field[y]){
      houses++;
    }
  }

  console.log(houses);

});
