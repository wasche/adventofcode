#!/usr/bin/env node

var data = require('./12.json');

var n = 0
  , q = [data]
  , o;

while (q.length){
  o = q.shift();
  for (k in o){
    if (typeof o[k] === 'number'){
      n += o[k];
    } else if (typeof o[k] === 'object'){
      q.push(o[k]);
    }
  }
}

console.log(n);
