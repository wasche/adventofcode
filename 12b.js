#!/usr/bin/env node

Object.values = function(o){
  return Object.keys(o).map(function(k){ return o[k]; });
};

var data = require('./12.json');

var n = 0
  , q = [data]
  , o;

while (q.length){
  o = q.shift();
  if (!Array.isArray(o) && Object.values(o).indexOf('red') >= 0) continue;
  for (k in o){
    if (typeof o[k] === 'number'){
      n += o[k];
    } else if (typeof o[k] === 'object'){
      q.push(o[k]);
    }
  }
}

console.log(n);
