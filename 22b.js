#!/usr/bin/env node

Object.clone = function(obj){
  var o = {};
  Object.keys(obj).forEach(function(key){
    o[key] = typeof obj[key] === 'object' ? Array.isArray(obj[key]) ? obj[key].slice() : Object.clone(obj[key]) : obj[key];
  });
  return o;
}

var spells = {
  magic_missle: {cost: 53, dmg: 4}
, drain: {cost: 73, dmg: 2, heal: 2}
, shield: {cost: 113, duration: 6, armor: 7}
, poison: {cost: 173, duration: 6, dmg: 3}
, recharge: {cost: 229, duration: 5, mana: 101}
};

// least mana spent and still win
var fights = [{
  order: []
, round: 0
, boss: 71
, dmg: 10
, player: 50
, mana: 500
, armor: 0
, timers: {}
, spent: 0
}];

var best;
var count = 0;

do{

  var fight = fights.shift();
  fight.round++;
  fight.armor = 0;

  if (fight.round % 2 === 1){
    fight.player--;
    if (fight.player <= 0) continue;
  }

  // handle any ongoing effects
  Object.keys(fight.timers)
  .forEach(function(t){
    var s = spells[t];
    fight.timers[t]--;
    if (s.armor) fight.armor = s.armor;
    if (s.dmg) fight.boss -= s.dmg;
    if (s.mana) fight.mana += s.mana;
    if (fight.timers[t] === 0) delete fight.timers[t];
  });

  if (fight.boss <= 0){
    if (!best || fight.spent < best.spent || (fight.spent = best.spent && fight.order.length < best.order.length)) best = fight;
    count++;
    continue;
  }

  // boss turn
  if (fight.round % 2 === 0){
    fight.player -= Math.max(1, fight.dmg - fight.armor);
    if (fight.player > 0) fights.push(fight);
    else {
      count++;
    }
    continue;
  }

  // player turn

  // pick a spell
  Object.keys(spells)
  .filter(function(spell){
    // not already on-going and have enough mana to cast
    return fight.mana >= spells[spell].cost && !fight.timers[spell];
  })
  .forEach(function(spell){
    var s = spells[spell];
    var f = Object.clone(fight);
    f.order.push(spell);
    f.mana -= s.cost;
    f.spent += s.cost;

    if (s.duration){
      f.timers[spell] = s.duration;
    } else {
      if (s.dmg) f.boss -= s.dmg;
      if (s.heal) f.player += s.heal;
    }

    if (f.boss <= 0){
      if (!best || f.spent < best.spent || (f.spent = best.spent && f.order.length < best.order.length)){
        best = f;
      }
      count++;
    } else {
      fights.push(f);
    }

  });

} while (fights.length);

console.dir(best);
console.log('Ran', count, 'scenarios.');
