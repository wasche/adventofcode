#!/usr/bin/env node

var ingredients = {};
function sum(a,b){ return a + b; }
function mul(a,b){ return Math.max(0, a) * Math.max(0, b); }

require('readline').createInterface(process.stdin, null)
.on('line', function(line){

  // Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3
  var m = line.match(/^(\w+): capacity (-?\d+), durability (-?\d+), flavor (-?\d+), texture (-?\d+), calories (-?\d+)$/);
  ingredients[m[1]] = {
    capacity: parseInt(m[2], 10)
  , durability: parseInt(m[3], 10)
  , flavor: parseInt(m[4], 10)
  , texture: parseInt(m[5], 10)
  , calories: parseInt(m[6], 10)
  };

})
.on('close', function(){

  var n = Object.keys(ingredients)
    , t = Array.apply(null, {length: 100 - n.length + 1})
            .map(function(v, i){ return i + 1; })
    , r
    , s = 0
    ;

  require('js-combinatorics')
    .baseN(t, n.length)
    .forEach(function(arr){
      if (arr.reduce(sum) !== 100) return;
      if (n.map(function(i, x){ return ingredients[i].calories * arr[x]; }).reduce(sum) !== 500) return;
      var score = ['capacity', 'durability', 'flavor', 'texture']
        .map(function(prop){
          return n.map(function(i, x){
            return ingredients[i][prop] * arr[x];
          })
          .reduce(sum);
        })
        .reduce(mul);
      if (score > s){
        s = score;
        r = arr;
        console.log(s);
      }
    });

  console.dir(r);

});
