#!/usr/bin/env node

var containers = [];

require('readline').createInterface(process.stdin, null)
.on('line', function(line){

  containers.push(parseInt(line, 10));

})
.on('close', function(){

  var shortest = Number.MAX_VALUE;

  console.dir(
    require('js-combinatorics')
      .power(containers)
      .filter(function(arr){
        if (arr.length && arr.reduce(function(a, b){ return a + b; }) === 150){
          if (arr.length < shortest) shortest = arr.length;
          return true;
        }
        return false;
      })
      .filter(function(arr){
        return arr.length === shortest
      })
      .length
  );

});
