#!/usr/bin/env node

var input = '1113222113';
var rounds = 50;

var read = function(n){
  return n.split('')
    .reduce(function(r, d, i, arr){
      if (r.length && d == r[r.length-1]){
        r[r.length-2]++;
      } else {
        r.push(1, d);
      }
      return r;
    }, [])
    .join('');
};

for (; rounds > 0; rounds--){
  input = read(input);
}

console.log(input.length);
