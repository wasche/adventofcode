#!/usr/bin/env node

var Combinatorics = require('js-combinatorics');

var boss = {
  hp: 104
, dmg: 8
, armor: 1
};

var weapons = {
  dagger:     {cost: 8, dmg: 4, armor: 0}
, shortsword: {cost: 10, dmg: 5, armor: 0}
, warhammer:  {cost: 25, dmg: 6, armor: 0}
, longsword:  {cost: 40, dmg: 7, armor: 0}
, greataxe:   {cost: 74, dmg: 8, armor: 0}
};

var armor = {
  none:       {cost: 0, dmg: 0, armor: 0}
, leather:    {cost: 13, dmg: 0, armor: 1}
, chainmail:  {cost: 31, dmg: 0, armor: 2}
, splintmail: {cost: 53, dmg: 0, armor: 3}
, bandedmail: {cost: 75, dmg: 0, armor: 4}
, platemail:  {cost: 102, dmg: 0, armor: 5}
};

var rings = {
  nol:  {cost: 0, dmg: 0, armor: 0}
, nor:  {cost: 0, dmg: 0, armor: 0}
, dmg1: {cost: 25, dmg: 1, armor: 0}
, dmg2: {cost: 50, dmg: 2, armor: 0}
, dmg3: {cost: 100, dmg: 3, armor: 0}
, def1: {cost: 20, dmg: 0, armor: 1}
, def2: {cost: 40, dmg: 0, armor: 2}
, def3: {cost: 80, dmg: 0, armor: 3}
};

// 1 weapon
// 0-1 armor
// 0-2 rings

// player, then boss, dmg - armor, first to 0 hp
// least amount of gold to win

// for each weapon
var g = Object.keys(weapons)
.map(function(w){ return weapons[w]; })
.map(function(w){
  // for each armor or lack of
  return Object.keys(armor)
  .map(function(a){ return armor[a]; })
  .map(function(a){
    return Combinatorics.combination(Object.keys(rings), 2)
    .map(function(rs){
      return [rings[rs[0]], rings[rs[1]]];
    })
    .filter(function(rs){
      var player = {
        hp: 100,
        dmg: w.dmg + a.dmg + rs[0].dmg + rs[1].dmg,
        armor: w.armor + a.armor + rs[0].armor + rs[1].armor
      };
      var win = Math.ceil(boss.hp / Math.max(1, player.dmg - boss.armor));
      var lose = Math.ceil(player.hp / Math.max(1, boss.dmg - player.armor));
      return win > lose;
    })
    .map(function(rs){
      return w.cost + a.cost + rs[0].cost + rs[1].cost;
    })
    .reduce(function(a,b){ return Math.max(a,b); }, 0);
  })
  .reduce(function(a,b){ return Math.max(a,b); }, 0);
})
.reduce(function(a,b){ return Math.max(a,b); }, 0);

console.log(g);
