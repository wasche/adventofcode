#!/usr/bin/env node

var target = {
  children: 3
, cats: 7
, samoyeds: 2
, pomeranians: 3
, akitas: 0
, vizslas: 0
, goldfish: 5
, trees: 3
, cars: 2
, perfumes: 1
};

var data = [];

require('readline').createInterface(process.stdin, null)
.on('line', function(line){

  // Sue 1: goldfish: 6, trees: 9, akitas: 0
  var m = line.match(/^Sue (\d+): ((?:\w+: \d+,? ?)+)$/)
    , q = m[2].match(/\w+: \d+/g)
            .map(function(s){
              var n = s.match(/^(\w+): (\d+)$/);
              return n.slice(1);
            })
    ;

  data.push(q.reduce(function(o, p){
      o[p[0]] = parseInt(p[1]);
      return o;
    },
    {_id: m[1]})
  );

})
.on('close', function(){

  data.filter(function(aunt){
    for (var q in aunt){
      if (q === '_id') continue;
      if (q === 'cats' || q === 'trees'){
        if (aunt[q] <= target[q]) return false;
      }
      else if (q === 'pomeranians' || q === 'goldfish'){
        if (aunt[q] >= target[q]) return false;
      }
      else if (aunt[q] != target[q]) return false;
    }
    return true;
  })
  .forEach(console.dir);

});
