#!/usr/bin/env node

var floor = 0;
var len = 0;

process.stdin.setEncoding('utf8');

process.stdin.on('readable', function(){
  var chunk = process.stdin.read();
  if (chunk !== null){
    for (var i = 0; i < chunk.length; i++){
      if (chunk[i] === '(') floor++;
      else if (chunk[i] === ')') floor--;
      len++;
      if (floor === -1){
        console.log(len);
        process.exit(0);
      }
    }
  }
});

process.stdin.on('end', function(){

});
