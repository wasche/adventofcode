#!/usr/bin/env node

var deer = {}
  , time = 2503
  ;

require('readline').createInterface(process.stdin, null)
.on('line', function(line){

  // Vixen can fly 19 km/s for 7 seconds, but then must rest for 124 seconds.
  var m = line.match(/^(\w+) can fly (\d+) km\/s for (\d+) seconds, but then must rest for (\d+) seconds.$/)
    , r = m[1]
    , speed = parseInt(m[2])
    , fly = parseInt(m[3])
    , rest = parseInt(m[4])
    ;

  deer[r] = {
    speed: speed
  , fly: fly
  , rest: rest
  };

})
.on('close', function(){

  var x = Object.keys(deer).reduce(function(s, n){
    var o = deer[n]
      , d = Math.floor(time / (o.fly + o.rest)) * o.speed * o.fly +
            Math.min(time % (o.fly + o.rest), o.fly) * o.speed
      ;
    return Math.max(d, s);
  }, 0);

  console.log(x);

});
