#!/usr/bin/env node

var Combinatorics = require('js-combinatorics');
var sum = function(a,b){ return a + b; }
var mul = function(a,b){ return a * b; }

var weights = [];

require('readline').createInterface(process.stdin, null)
.on('line', function(line){

  weights.push(parseInt(line,10));

})
.on('close', function(){

  // need to divide in thirds, so first find sum
  var total = weights.reduce(sum);

  if (total % 3 !== 0){ console.log('not evenly divisible!'); return; }

  var target = total / 3;

  // find all combinations of the smallest set that sum to the target
  var sets = [];
  var qe = Number.MAX_VALUE;

  for (var i = 1; i < weights.length - 2 && !sets.length; i++){
    Combinatorics.combination(weights, i)
    .filter(function(arr){
      return arr.reduce(sum) === target;
    })
    .forEach(function(arr){
      sets.push(arr);
      qe = Math.min(qe, arr.reduce(mul));
    });
  }

  sets = sets.filter(function(s){
    return s.reduce(mul) === qe;
  });

  console.log(qe);

});
