#!/usr/bin/env node

var sqft = 0;

require('readline').createInterface(process.stdin, null)
.on('line', function(line){
  var dims = line.trim().split('x')
   , l = parseInt(dims.shift())
   , w = parseInt(dims.shift())
   , h = parseInt(dims.shift())
   , sides = [l, w, h].sort(function(a,b){ return a - b; })
   , a = sides[0]
   , b = sides[1]
   , v = l * w * h
   , s = 2 * (a + b) + v
   ;
   sqft += s;
})
.on('close', function(){
  console.log(sqft);
});
